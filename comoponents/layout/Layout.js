import React from "react";
import MeetupNavigation from "./MeetupNavigation";

const Layout = (props) => {
  return (
    <>
      <MeetupNavigation />
      <main>{props.children}</main>
    </>
  );
};

export default Layout;
