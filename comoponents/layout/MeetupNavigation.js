import Link from "next/link";

const MeetupNavigation = () => {
  return (
    <header>
      <div>
        <h1>Meetups App</h1>
      </div>
      <nav>
        <ul>
          <li>
            <Link href="/">All Meetups</Link>
          </li>
          <li>
            <Link href="/new-meetup">Add a meetups</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default MeetupNavigation;
