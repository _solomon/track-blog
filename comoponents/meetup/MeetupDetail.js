import React from "react";

const MeetupDetail = ({ address, title, description }) => {
  return (
    <>
      <h1>{title}</h1>
      <p>{description}</p>
      <address>{address}</address>
    </>
  );
};

export default MeetupDetail;
