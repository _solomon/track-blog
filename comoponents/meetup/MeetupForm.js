import { useRef } from "react";

function MeetupForm(props) {
  const titleRef = useRef();
  const descriptionRef = useRef();
  const addressRef = useRef();

  const handleSubmitForm = (event) => {
    event.preventDefault();

    const title = titleRef.current.value;
    const address = addressRef.current.value;
    const description = descriptionRef.current.value;

    const meetupData = {
      title,
      address,
      description,
    };

    props.onAddMeetup(meetupData);
  };

  return (
    <>
      <form onSubmit={handleSubmitForm}>
        <div>
          <label htmlFor="title">Title</label>
          <input type="text" name="title" required ref={titleRef} />
        </div>
        <div>
          <label htmlFor="address">Address</label>
          <input type="text" name="address" required ref={addressRef} />
        </div>
        <div>
          <label htmlFor="description">Description</label>
          <textarea
            name="description"
            id=""
            cols="30"
            rows="10"
            ref={descriptionRef}
          ></textarea>
        </div>
        <button>Add Meetup</button>
      </form>
    </>
  );
}

export default MeetupForm;
