import { useRouter } from "next/router";

const MeetupItem = ({ title, description, address, id }) => {
  const router = useRouter();

  const handleShowDetails = () => {
    router.push("/" + id);
  };

  return (
    <ul>
      <li>
        <h2>{title}</h2>
      </li>
      <li>
        <h4>{description}</h4>
      </li>
      <li>
        <p>{address}</p>
      </li>
      <div>
        <button onClick={handleShowDetails}>Show details</button>
      </div>
    </ul>
  );
};

export default MeetupItem;
