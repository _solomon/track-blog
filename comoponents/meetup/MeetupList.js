import MeetupItem from "./MeetupItem";

const MeetupList = ({ meetups }) => {
  const meetupText = meetups.map((meetup) => (
    <MeetupItem
      title={meetup.title}
      description={meetup.description}
      address={meetup.address}
      id={meetup.id}
      key={meetup.id}
    />
  ));

  return meetupText;
};

export default MeetupList;
