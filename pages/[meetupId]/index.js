import React from "react";
import MeetupDetail from "../../comoponents/meetup/MeetupDetail";

const MeetupId = ({ meetup }) => {
  return (
    <MeetupDetail
      id={meetup.id}
      title={meetup.title}
      address={meetup.address}
      description={meetup.description}
    />
  );
};

export async function getStaticPaths() {
  return {
    fallback: false,
    paths: [
      {
        params: {
          meetupId: "m1",
        },
      },
    ],
  };
}

export async function getStaticProps() {
  return {
    props: {
      meetup: {
        id: "m1",
        title: "NextJs",
        address: "resouce center",
        description:
          "This is a meetup for developer who want to learn about the react framework Nextjs for production",
      },
    },
  };
}

export default MeetupId;
