import MeetupList from "../comoponents/meetup/MeetupList";
import { DUMMY_DATA } from "../dummy_data";

const HomePage = ({ meetups }) => {
  return <MeetupList meetups={meetups} />;
};

export async function getStaticProps() {
  return {
    props: {
      meetups: DUMMY_DATA,
    },
    revalidate: 3600,
  };
}

export default HomePage;
